package main

import (
	"dtcloud-api/routers"
	"net/http"

	"github.com/gin-gonic/gin"
	// "gorm.io/driver/postgres"
	// "gorm.io/gorm"
	// "gorm.io/driver/postgres"
	// "gorm.io/gorm"
)

func main() {
	r := gin.Default()

	// 设置静态目录 相对路径写入运行环境，以便调用
	r.LoadHTMLGlob("./Template/website/zh_CN/html/**/*")
	// 引用图片资源 第一个参数为网址路由，第二个参数为静态资源实际位置
	r.Static("/img", "./Template/website/zh_CN/static/img")
	// 引用图片资源  第一个参数为网址路由，第二个参数为静态资源实际位置
	r.Static("/css", "./Template/website/zh_CN/static/css")

	routers.ApiRoutersInit(r)

	r.GET("/index", func(context *gin.Context) {
		// fullPath := "请求路径：" + context.FullPath()
		// fmt.Println(fullPath)

		context.HTML(http.StatusOK, "admin/index.html", gin.H{
			"title": "DTCloud",
		})

	})

	// //打印链接的额数据库的参数信息
	// driverName := "Ò"
	// host := "localhost"
	// port := "5432"
	// database := "DTCloud15"
	// username :=
	// password := '123456'

	r.Run(":8084")
}
