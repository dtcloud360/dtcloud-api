package routers

import (
	"dtcloud-api/appstore/api"

	"github.com/gin-gonic/gin"
)

func ApiRoutersInit(r *gin.Engine) {
	apiRouters := r.Group("/api")
	{
		//注意接口首字母要大写
		apiRouters.GET("/create", api.ApiController{}.Create)

	}

}
